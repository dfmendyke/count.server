# Time-stamp: <2020-12-29 15:33:10 daniel>

##
# Rules

# Create dependent list from a CC source file
%.dd : %.cc ; $(cc) $(ccflgs) -c -MM -MT $(<:.cc=.o) -MT $(<:.cc=.dd) -MF $(<:.cc=.dd) $<

# Compile a CC file into an object file
%.o : %.cc ; $(cc) $(ccflgs) -c -o $@ $<
