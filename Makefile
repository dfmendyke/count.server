# Time-stamp: <2020-12-29 15:28:48 daniel>
# Makefile used with the Counting Server
# Daniel Mendyke [daniel at mendyke.com]

##
# Tools
e := /bin/env
cc := $(e) g++
remove := $(e) rm
linker := $(e) ar

##
# Options
ccflgs := -g -std=c++20 -I./include -I./stone

##
# Make rules
include rules.mk

##
# Variables
target := counting-server
source =

##
# Modules
module := src
include $(addsuffix /module.mk,$(module))

##
# Objects, Source and Dependents
dependents += $(addsuffix .dd,$(source))
objects += $(addsuffix .o,$(source))


##
# Main target
all : $(target) $(dependents) ; @echo Build Complete

##
# Insure dependencies are maintain
include $(dependents)

##
# Application
$(target) : $(objects) ; $(cc) $(ccflgs) -o $@ $^

##
# Execute the target
.PHONY : run
run : ; @./$(target)

##
# Remove build artifacts
.PHONY : clean
clean : ; $(remove) --force $(objects) $(dependents) $(target)
