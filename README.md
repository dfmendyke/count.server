Time-stamp: <2023-01-21 09:32:43 daniel>

**COUNTING SERVER**

by Daniel [daniel@mendyke.com]


OVERVIEW

  The Counting Server is a simple application that maintains a constant
  count of increments and decrements applied to it by networked clients.
  The default number of connections is 1024, although this can be changed
  in the source code if desired (include/socket.hh).

  _Note.  The Counting Server only operates using IPv4 addressing._


BUILDING

  Standard GNUMake is needed to build.  From the root directory of the
  project issue the following command:

    make

  Building requires the g++ version 10.2.0 or higher using C++20.


RUNNING

  The server uses port 8089 as a default.  Once the server has been
  built it can be run by executing the command:

    ./server

  Connecting to the server can be done using telnet with the command:

    telnet [host] 8089

  Where [host] is the DNS name of IP address of the system where the
  server is being executed.  Normally 'localhost' if it is the same
  machine.


COMMANDS

  The Counting Server recognizes the following commands:

    INCR [N]
    DECR [N]
    OUTPUT

  Where 'N' is either a positive or negative integer.  Upon receiving
  command the Counting Server will update it's total count and then
  send the new count to all other connections.
  The command OUTPUT will only send the total count to the connection
  that issued the command.


SIGTERM

   The Counting Server also watches for the system signal SIGTERM.
   Upon receiving this signal the server send the total count
   to all connections as 'FINAL COUNT IS', closes all active connections
   and then exits with code 0.

   Note that there is a 10 second time out used with the pselect.
   It may take the full 10 seconds after sending the signal SIGTERM
   before the signal is actually handled.

   You will need the PID of the running server when using the 'kill'
   command.  An easy way to send this signal, from the same
   machine it is running on, is to issue the following command:

    PID=`pidof server` && kill -s SIGTERM ${PID}


SYSTEMD

  The project file 'counting-server.service' might be useful as
  a systemd init script for the Counting Server.  It is suggested
  that you copy the server binary, counting-server, to /usr/local/bin.
  Copy 'counting-server.service' to /etc/systemd/system/ and use chmod
  to set the permissions to '644'.
