// Time-stamp: <2021-01-03 21:40:58 daniel>
#ifndef __SOCKET_HH__
#define __SOCKET_HH__

// socket.hh

//-----------------------------------------------------------------------------
#include <stdexcept>
#include <sys/types.h>
#include <netinet/in.h>



//-----------------------------------------------------------------------------
namespace counting {


  // Wrapper around a socket object - used for ease of error handling
  //---------------------------------------------------------------------------
  class socket {

  public:

    socket( int, int = socket::default_backlog );
    virtual ~socket( );

    int id() { return handle_; };

    void setsockopt( int = 1 );
    void ioctl( int = 1 );
    void bind();
    void listen();
    int accept( struct sockaddr* );

  protected:

    void socket_( int );
    class exception;

  private:

    int handle_;  // id of this socket
    int connection_;  // the handle of the socket during a connection
    int backlog_;  // how many active connections

    sockaddr_in sock_;
    sockaddr* sock_addr_;

    static const int default_backlog = 1024; // Max number of connects

  };  // end socket


  // Socket Exception class
  //---------------------------------------------------------------------------
  class socket::exception : public std::runtime_error {

  public:

    exception( const char* );

  };  // end socket::exception


};  // end NS density


#endif  // __SOCKET_HH__
