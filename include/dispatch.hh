// Time-stamp: <2020-12-28 19:43:41 daniel>
// dispatch.hh - handle server commands
#ifndef __DISPATCH_HH__
#define __DISPATCH_HH__

// dispatch.hh

//-----------------------------------------------------------------------------
#include <map>
#include <string>


//-----------------------------------------------------------------------------
namespace counting {


  // Stores all valid commands in a searchable map with a string object
  // for the key and a command object for the value.
  //---------------------------------------------------------------------------
  class dispatch_table {

  public:

    enum class command { notfound, increment, decrement, output, error };

    dispatch_table();
    virtual ~dispatch_table();

    dispatch_table::command parse( const std::string& ) const;

  private:

    std::map< std::string, command > map_;

  };  // end dispatch_table


};  // end NS counting


#endif  // __DISPATCH_HH__
