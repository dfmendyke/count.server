// Time-stamp: <2020-12-28 19:42:35 daniel>
#ifndef __CODE_HH__
#define __CODE_HH__

// code.hh

//-----------------------------------------------------------------------------
namespace global {

  // Convient wrapper for return codes.  Used to avoid 'magic' numbers
  // in course.  Can not use enum class as library calls return type 'int'.
  //---------------------------------------------------------------------------
  struct code {
    static const int ok = 0;
    static const int error = -1;
  };  // end code

};  // end NS system

#endif  // __CODE_HH__
