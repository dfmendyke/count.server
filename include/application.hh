// Time-stamp: <2020-12-31 09:05:45 daniel>
#ifndef __APPLICATION_HH__
#define __APPLICATION_HH__

// application.hh

//-----------------------------------------------------------------------------
#include <sys/select.h>  // fd_set
#include <netinet/in.h>
#include "socket.hh"  // counting::socket
#include "sig.hh"  // sig::handler
#include "timeout.hh"
#include "dispatch.hh"  // dispatch table for running incoming commands

//-----------------------------------------------------------------------------
namespace counting {

  // A complex class that in a commercial release should be broken up
  // into several classes and added to 'application' as member variables.
  // This class uses 'pselect' to monitor multiply connections for
  // incoming commands, executes them and responds.
  // It also watches for an incoming SIGTERM and sends data to all
  // active connections.
  //---------------------------------------------------------------------------
  class application {

  public:

    application();
    virtual ~application();

    int main();
    void term_caught();

    static const int port = 8089;  // port to use for this application

  protected:

    // utility
    void initialize_socket();  // call bind and listen
    void copy_buffer( const char*, size_t );  // copy formated text to buffer
    void format_total_count();  // copy the total count into buffer_
    void format_closing_message();  // copy the closing message to buffer_
    void format_error();  // send an error message back
    void parse_command( int );  // deal with a connection send command

    // connections
    void poll_select();  // add error handling to system pselect function
    void socket_is_set( int );  // passed socket handle is live
    void accept_connection( );  // deal with a new connection
    void respond( int );
    int receive( int );  // read bytes from connected socket
    void send( int );  // send data out to the remote connection
    void close_fd( int );  // close the file handle

    // ouput
    void report_connection();  // output deals of the connection

  private:


    static const int seconds = 10;  // seconds to wait for a connection
    static const int buffer_length = 256;  // size of incoming buffer

    counting::dispatch_table dispatch_;  // holds valid commands
    struct sockaddr remote_;
    counting::socket socket_;
    fd_set master_;  // Master list of socket descriptors
    fd_set working_;  // Copied when needed form master_
    sig::handler sigterm_;
    global::timeout timeout_;
    char buffer_[ application::buffer_length ];
    int maxsock_;  // max socket descriptor number
    int connection_;  // used when accessing a new connection
    int byte_count_;
    int total_count_;  // holds the actual count
    bool continue_looping_;  // main loop flag

  };  // end application



};  // end NS counting


#endif  // __APPLICATION_HH__
