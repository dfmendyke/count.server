// Time-stamp: <2020-12-28 19:45:11 daniel>
#ifndef __SIG_HH__
#define __SIG_HH__

// sig.hh

//-----------------------------------------------------------------------------
#include <signal.h>


// Forward dec
namespace counting { class application; };


//-----------------------------------------------------------------------------
namespace sig {


  // Wrapper around a signal handler.
  // Uses a static member function as the action.
  // Stores a copy of the object pointer and a copy of the calling
  // parent object so that a callback function can be executed upon
  // receiving the handled signal.
  //---------------------------------------------------------------------------
  class handler {

  public:

    handler( counting::application*, int );
    virtual ~handler() { };
    sigset_t* set() { return &set_; };
    int handle() const { return handle_; };
    counting::application* parent() { return parent_; };

    static handler* self() { return handler::self_; };
    static void func( int );

  private:

    sigset_t set_;
    int handle_;
    static handler* self_;
    static counting::application* parent_;

  };  // end handler

};  // end NS sig


#endif  // __SIG_HH__
