// Time-stamp: <2020-12-28 19:46:14 daniel>
#ifndef __TIMEOUT_HH__
#define __TIMEOUT_HH__

//-----------------------------------------------------------------------------
#include <time.h>

//-----------------------------------------------------------------------------
namespace global {

  // Used to make initalizing the timespec object easier in the
  // parent object or function.
  //---------------------------------------------------------------------------
  struct timeout {

    timeout( int );  // time in seconds
    struct timespec spec;

  };  // end timeout

};  // end NS global

#endif  // __TIMEOUT_HH__
