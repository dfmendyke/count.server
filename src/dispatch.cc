// Time-stamp: <2020-12-28 19:37:44 daniel>
// dispatch.cc

//-----------------------------------------------------------------------------
#include <sstream>
#include "dispatch.hh"

//-----------------------------------------------------------------------------
using std::string;
using counting::dispatch_table;


// construct the table by putting valid commands into the member map
//-----------------------------------------------------------------------------
dispatch_table::dispatch_table()
  : map_{
      { "INCR", command::increment },
      { "DECR", command::decrement },
      { "OUTPUT", command::output } } {
};  // end constructor


//-----------------------------------------------------------------------------
dispatch_table::~dispatch_table() { };  // end destructor


// find the command stored by string
//-----------------------------------------------------------------------------
dispatch_table::command dispatch_table::parse( const string& instruction ) const {

  auto iter = map_.find( instruction );
  if ( iter == map_.end() ) return command::notfound;
  return iter->second;

};  // end parse
