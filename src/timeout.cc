// Time-stamp: <2020-12-28 19:34:18 daniel>
// timeout.cc


//-----------------------------------------------------------------------------
#include "timeout.hh"

//-----------------------------------------------------------------------------
using global::timeout;

// Fills the timespec struct with number of seconds before time out.
//-----------------------------------------------------------------------------
timeout::timeout( int seconds ) : spec() {

  spec.tv_sec = seconds;
  spec.tv_nsec = 0;

};  // end constructor
