// Time-stamp: <2020-12-28 19:36:47 daniel>
// sig.cc

//-----------------------------------------------------------------------------
#include <string>
#include <iostream>
#include "sig.hh"
#include "application.hh"

//-----------------------------------------------------------------------------
using std::string;
using sig::handler;
using counting::application;
using std::cerr;
using std::endl;

// Static values used by the signal handler
//-----------------------------------------------------------------------------
handler* handler::self_ = nullptr;
counting::application* handler::parent_ = nullptr;

// Constructor
//-----------------------------------------------------------------------------
handler::handler( application* parent, int signum )
  : set_(), handle_( signum ) {

  struct sigaction action;
  struct sigaction info;

  // Set the action to point to the static member function
  action.sa_handler = handler::func;
  action.sa_flags = 0;
  ::sigaction( signum, &action, &info );
  ::sigemptyset( &set_ );
  ::sigaddset( &set_, SIGTERM );

  handler::self_ = this;
  handler::parent_ = parent;

};  // end constructor

// Called by the system when a signal is caught by the application.
// specifically calls 'term_caught' but a more general approach would
// be to store a callback function.
//-----------------------------------------------------------------------------
void handler::func( int signum ) {

  string sig{ signum == SIGTERM ? "SIGTERM" : "UNKNOWN" };
  cerr << "Signal caught: " << sig << endl;
  if ( signum == handler::self()->handle() )
    handler::self()->parent()->term_caught();

};  // end func
