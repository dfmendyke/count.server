// Time-stamp: <2020-12-28 19:26:38 daniel>
// socket.cc - part of the Density Counting Server

//-----------------------------------------------------------------------------
#include "socket.hh"
#include "code.hh"
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <cstring>

//-----------------------------------------------------------------------------
using counting::socket;


// Constructor
//-----------------------------------------------------------------------------
socket::socket( int port, int backlog )
  : handle_{ 0 }, connection_{ 0 }, backlog_{ backlog },
    sock_(), sock_addr_( reinterpret_cast< sockaddr* >( &sock_ ) ) {

  memset( &sock_, '0', sizeof( sockaddr_in ) );
  socket_( port );  // Create the socket

};  // end constructor


// Destructor
//-----------------------------------------------------------------------------
socket::~socket() { };


//-----------------------------------------------------------------------------
void socket::socket_( int port ) {

  handle_ = ::socket( AF_INET, SOCK_STREAM, 0 );
  if ( handle_ == global::code::error )
    throw socket::exception( "Error: Call to function 'socket' failed!" );

  sock_.sin_family = AF_INET;
  sock_.sin_addr.s_addr = htons( INADDR_ANY );
  sock_.sin_port = htons( port );

};  // end socket_


//-----------------------------------------------------------------------------
void socket::setsockopt( int option ) {

  auto result = ::setsockopt( handle_, SOL_SOCKET, SO_REUSEADDR, &option, sizeof( option ) );
  if ( result == global::code::error )
    throw socket::exception( "Error: 'setsockopt' func failed!" );

};  // end setsockopt_


//-----------------------------------------------------------------------------
void socket::ioctl( int option ) {

  auto result = ::ioctl( handle_, FIONBIO, &option );
  if ( result == global::code::error )
    throw socket::exception( "Error: 'ioctl' func failed!" );

};  // end ioctl_

//-----------------------------------------------------------------------------
void socket::bind( ) {

  if ( ::bind( handle_, sock_addr_, sizeof( sock_ ) ) == global::code::error )
    throw socket::exception( "Error: Cannot bind the socket!" );

};  // end bind_

//-----------------------------------------------------------------------------
void socket::listen( ) {

  if ( ::listen( handle_, backlog_ ) == global::code::error )
    throw socket::exception( "Error: listen function failed!" );

};  // end listen_


//-----------------------------------------------------------------------------
int socket::accept( struct sockaddr* addr ) {

  socklen_t length = sizeof( addr );
  int connection = ::accept( handle_, addr, &length );
  if ( connection == global::code::error )
    throw socket::exception( "Error: Func 'accept' failure!" );
  return connection;

};  // end accept

//-----------------------------------------------------------------------------
socket::exception::exception( const char* message )
  : runtime_error( message ) { };
