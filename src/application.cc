// Time-stamp: <2023-01-21 09:29:06 daniel>
// application.cc

// Uses the 'pselect' command to monitor a number of active connections
// (default is 20).  Maintains a total count and increases or decreases
// that count based on incomming commands from the connections.
// See the project README.md file for details.


// Header files
//-----------------------------------------------------------------------------
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include "application.hh"
#include "socket.hh"  // counting::socket
#include "code.hh"


//-----------------------------------------------------------------------------
using counting::application;
using counting::socket;
using std::string;
using std::cerr;
using std::endl;
using std::stringstream;

// Construct the main object - Needs more abstraction of members
//-----------------------------------------------------------------------------
application::application()
  : dispatch_(), remote_(), socket_( application::port ),
    master_(), working_(), sigterm_{ this, SIGTERM },
    timeout_{ application::seconds },
    maxsock_{ 0 },
    connection_{ 0 },
    byte_count_{ 0 },
    total_count_{ 0 },
    continue_looping_{ true } {

  FD_ZERO( &master_ );
  FD_ZERO( &working_ );
  initialize_socket();

};  // end constructor

//-----------------------------------------------------------------------------
application::~application() {

};  // end destructor



// convert a sockaddr object into a human readable IP address and
// stores it into a std::string object.  Currently only support IPv4.
//-----------------------------------------------------------------------------
string addr_string( struct sockaddr* addr ) {

  char buffer[ INET_ADDRSTRLEN + 1 ];
  struct sockaddr_in* addr_in = (struct sockaddr_in*)addr;

  inet_ntop( AF_INET, &addr_in->sin_addr, buffer, INET_ADDRSTRLEN );
  return string{ buffer };

};  // end add_string


// Copy the text into buffer_ and set byte_Count_
//-----------------------------------------------------------------------------
void application::copy_buffer( const char* message, size_t length ) {

  byte_count_ = length;
  memset( buffer_, 0x0, application::buffer_length - 1 );
  strncpy( buffer_, message, byte_count_ );

};  // end copy_buffer


// Copy the formated total count to buffer_.
// Look into using the std::fmt function for quicker string formating.
// The compiler does not yet support this feature but there are several
// external libraries that provide ideal implementations.
//-----------------------------------------------------------------------------
void application::format_total_count() {

  stringstream stream;
  stream << "COUNT IS " << total_count_ << endl;
  copy_buffer( stream.str().c_str(), stream.str().size() );
  //  byte_count_ = stream.str().size();
  //memset( buffer_, 0x0, application::buffer_length -1 );
  //strncpy( buffer_, stream.str().c_str(), byte_count_ );

};  // end send_count

// Copy into buffer_ this error message.
//-----------------------------------------------------------------------------
void application::format_error() {

  static const char* error = "UNKNOWN COMMAND\n";
  copy_buffer( error, strlen( error ) );
  //byte_count_ = strlen( error );
  //memset( buffer_, 0x0, application::buffer_length - 1 );
  //strncpy( buffer_, error, byte_count_ );

};  // end format_error

// Copy closing message to the buffer
//-----------------------------------------------------------------------------
void application::format_closing_message() {

  stringstream stream;
  stream << "FINAL COUNT IS " << total_count_ << endl << "BYE" << endl;
  copy_buffer( stream.str().c_str(), stream.str().size() );
  // byte_count_ = stream.str().size();
  // memset( buffer_, 0x0, application::buffer_length -1 );
  // strncpy( buffer_, stream.str().c_str(), byte_count_ );

};  // end format_closing_message


// Convert a string to all upper case
//-----------------------------------------------------------------------------
string str_toupper( string ss ) {
  std::transform( ss.begin(), ss.end(), ss.begin(),
                  []( unsigned char c ){ return std::toupper( c ); }
                  );
  return ss;
};  // end str_toupper


// Handle each supported command from an active connection.
//-----------------------------------------------------------------------------
void application::parse_command( int fd ) {

  stringstream stream{ buffer_ };
  string instruction;
  int amount{ 0 };
  stream >> instruction >> amount;
  instruction = str_toupper( instruction );  // ignore case


  switch ( dispatch_.parse( instruction ) ) {
  case dispatch_table::command::notfound:
    format_error();
    send( fd );
    return;  // don't update all connections
  case dispatch_table::command::increment:
    total_count_ += amount;
    break;
  case dispatch_table::command::decrement:
    total_count_ -= amount;
    break;
  case dispatch_table::command::output:
    format_total_count();
    send( fd );
    return;
  };  // end switch

  respond( fd );  // send the total count to all connections

};  // end parse_command


// Print the information on a new connections.
//-----------------------------------------------------------------------------
void application::report_connection( ) {

  cerr << "New connection from "
       << addr_string( &remote_ )
       << " on socket "
       << connection_
       << endl;

};  // end report_connection



// Wrapper around the 'pselect' statement.  This is a good candidate
// for further abstraction into it's own class and object.
//-----------------------------------------------------------------------------
void application::poll_select() {

  int result = ::pselect(
                         maxsock_ + 1,
                         &working_,
                         NULL,
                         NULL,
                         &timeout_.spec,
                         sigterm_.set()
                         );  // end pselect
  if ( result == global::code::error )
    throw std::runtime_error( "ERROR: 'pselect' failed!" );

};  // end poll_select


// Initialize the new connection
//-----------------------------------------------------------------------------
void application::accept_connection() {

  connection_ = socket_.accept( &remote_ );
  report_connection();  // print the IP and socket number of this connection
  FD_SET( connection_, &master_ ); // add to master set
  if ( connection_ > maxsock_ ) maxsock_ = connection_;

};  // end new_connection


//-----------------------------------------------------------------------------
void application::close_fd( int fd ) {

  close( fd );  // close handle
  FD_CLR( fd, &master_ );  // remove this connection from the master list
  cerr << "Socket " << fd << " has disconnected." << endl;

};  // end close_fd


//-----------------------------------------------------------------------------
int application::receive( int fd ) {

  memset( buffer_, 0x0, application::buffer_length );
  int count = ::recv( fd, buffer_, application::buffer_length, 0 );
  if ( count == global::code::error )  // count is -1
    throw std::runtime_error( "ERROR: 'recv' failed" );
  if ( count == global::code::ok ) close_fd( fd );  // count is zero
  return count;

};  // end receive


//-----------------------------------------------------------------------------
void application::send( int fd ) {

  int result = ::send( fd, buffer_, byte_count_, 0 );
  if ( result == global::code::error )
    cerr << "'send' to " << fd << " failed." << endl;
  //    throw std::runtime_error( "ERROR: 'send' failed!" );

};  // end send

// send to all other connections
//-----------------------------------------------------------------------------
void application::respond( int fd ) {

  format_total_count();  // copy the total count into buffer_
  for ( int handle = 0; handle <= maxsock_; ++handle ) {
    if ( FD_ISSET( handle, &master_ ) ) {
      if ( handle != socket_.id() && handle != fd ) send( handle );
    };  // end if FD_ISSET
  };  // end for loop

};  // end respond


// Deal with an live connection
//-----------------------------------------------------------------------------
void application::socket_is_set( int fd ) {

  if ( fd == socket_.id() ) {  // this is a new connection
    accept_connection();
    return;
  };  // end if fd is equl socket id

  // an old connect is ready
  byte_count_ = receive( fd );  // read data from the connection
  if ( byte_count_ > 0 ) parse_command( fd );

};  // end socket_is_set


// Init the socket after is has been open
//-----------------------------------------------------------------------------
void application::initialize_socket() {

  socket_.setsockopt();  // allow socket reuse
  socket_.bind();
  socket_.listen();  // listen upto socket::backlog_ number of connections

  FD_SET( socket_.id(), &master_ );
  maxsock_ = socket_.id();  // keep track of handles, start at socket id

};  // end initialize_socket


//-----------------------------------------------------------------------------
void application::term_caught() {

  format_closing_message();
  for ( int fd = 0; fd <= maxsock_; ++fd ) {
    if ( FD_ISSET( fd, &master_ ) && fd != socket_.id() ) {
      send( fd );
      close_fd( fd );
    };  // end if fd is set
  };  // end for loop

  continue_looping_ = false;  // exit loop and exit papplication

};  // end term_caught


// Run the server
//-----------------------------------------------------------------------------
int application::main( ) {

  // main loop
  while( continue_looping_ ) {

    working_ = master_; // copy master to working
    poll_select();  // call pselect to monitor max number of connections

    // run through the existing connections looking for data to read
    for( int fd = 0; fd <= maxsock_; ++fd )
      if ( FD_ISSET( fd, &working_ ) ) socket_is_set( fd );

  } // END while loop

  return global::code::ok;

};  // end main
